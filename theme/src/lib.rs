use raylib::prelude::*;
use fm::*;

pub mod config;
pub use crate::config::*;

pub fn load_background(rl: &mut RaylibHandle, thread: &RaylibThread) -> Option<Texture2D> {
    if !ENABLE_BG {
        return None;
    }

    // TODO: handle stuff such as cropping image, blurring image.
    match rl.load_texture(&thread, BG_IMG) {
        Ok(texture) => Some(texture),
        Err(_) => None,
    }
}

pub fn draw_background(d: &mut RaylibDrawHandle, img: &Texture2D, tasogare: &Tasogare) {

    match BG_FIT {
        FittingStrategy::Resize => {
            d.draw_texture_pro(
                img,
                Rectangle::new(0.0, 0.0, img.width as f32, img.height as f32),
                Rectangle::new(0.0, 0.0, tasogare.width as f32, tasogare.height as f32),
                Vector2::new(0.0, 0.0),
                0.0,
                Color::WHITE
            )
        }

        FittingStrategy::RatioScale => {
            let scale = if tasogare.width < img.width {
                tasogare.width as f32 / img.width as f32
            } else {
                img.width as f32 / tasogare.width as f32
            };

            d.draw_texture_pro(
                img,
                Rectangle::new(0.0, 0.0, img.width as f32, img.height as f32),
                Rectangle::new(0.0, 0.0, tasogare.width as f32, img.height as f32 * scale),
                Vector2::new(0.0, 0.0),
                0.0,
                Color::WHITE
            )
        }

        _ => d.draw_texture(img, 0, 0, Color::WHITE),
    }

    // Draw a black transparant rectangle on top of the background
    // image. Couldn't figured a better way to handle background dim.
    if BG_DIM > 0.0 {
        d.draw_rectangle(
            0,
            0,
            tasogare.width,
            tasogare.height,
            DIM.fade(BG_DIM)
        );
    }
}
