# TASOGARE
File manager written in Rust.

## Compiling
To compile `tasogare`, you have to move the `config_template.rs` file to
`theme/src/config.rs` and edit some values to fit your preferences.
After that you can compile `tasogare` by running:
```bash
cargo run --release
```

## Configuration, theming
`tasogare` is configured by editting the source code (the `theme/src/config.rs` file)
and then recompile. When a config file parser doesn't have to be implemented,
I have more time for meaningful things, and the program has faster start up time.

List of config values:
- `INITIAL_WW`, `INITIAL_WH`: initial window size when starting up.
- `BG`: background color.
- `FG`: text color.
- `DIM`: background image dim, ranges from 0.0 to 1.0.
- `ENABLE_BG`: is background image enabled?
- `BG_IMG`: path to background image file (must be a PNG).
- `BG_FIT`: how should `tasogare` fit the image on screen (see `enum FittingStrategy`).
- `FONT`: path to font. Can be TTF or OTF.
