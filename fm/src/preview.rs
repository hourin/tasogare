use std::{fs::read_to_string, path::PathBuf};

use raylib::prelude::*;

/// Collection for preview data. For now it can
/// preview Image and Text (that's enough I guess)
pub enum Preview {
    Image(Texture2D),
    Text(String),
}

impl Preview {
    /// Generate preview data.
    pub fn gen(
        rl: &mut RaylibHandle,
        thread: &RaylibThread,
        entry: Option<&PathBuf>
    ) -> Option<Preview> {
        // Without this, sometimes the program just segfaults.
        if let None = entry {
            return None;
        }

        let entry = entry.unwrap();

        if entry.extension() == None {
            match read_to_string(entry) {
                Ok(text) => { return Some(Preview::Text(text)); }

                Err(_) => { return None; },
            }
        }

        let texture = rl.load_texture(thread, entry.to_str().unwrap_or(""));
        match texture {
            Ok(texture) => Some(Preview::Image(texture)),

            Err(_) => {
                match read_to_string(entry) {
                    Ok(text) => Some(Preview::Text(text)),

                    Err(_) => None,
                }
            }
        }
    }
}
