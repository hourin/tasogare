use raylib::prelude::*;

use crate::Preview;

/// This struct holds the data for the Tasogare window
/// (are hidden files shown, which file is currently selected, etc.)
#[derive(Default)]
pub struct Tasogare {
    pub show_hidden: bool,
    pub selection: Vec<i64>,

    /// font doesn't fit here much, but it doesn't fit anywhere also.
    pub font: Option<Font>, // :troll:

    pub width: i32,
    pub height: i32,

    pub preview: Option<Preview>,
}

impl Tasogare {
    /// Update the width and height values when window is resized.
    pub fn update_size(&mut self, rl: &RaylibHandle) {
        self.width = rl.get_screen_width();
        self.height = rl.get_screen_height();
    }

    /// Load a font to Tasogare. This should belong to theme?
    pub fn load_font(&mut self, rl: &mut RaylibHandle, thread: &RaylibThread, name: &str) {
        self.font = Some(
            rl.load_font_ex(
                &thread,
                name,
                30,
                FontLoadEx::Default(0)
            ).unwrap()
        );

        {
            let tmp = self.font.as_ref().unwrap();
            tmp.texture()
                .set_texture_filter(
                    &thread,
                    TextureFilter::TEXTURE_FILTER_BILINEAR
            );
        }
    }

    pub fn latest_selection(&self) -> i64 {
        self.selection[self.selection.len()-1]
    }

    pub fn change_selection(&mut self, delta: i64, max: i64) {
        let last = self.selection.len()-1;
        self.selection[last] += delta;

        if self.selection[last] < 0 {
            self.selection[last] = max;
        } else if self.selection[last] > max {
            self.selection[last] = 0;
        }
    }
}
