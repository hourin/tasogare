// COPY THIS FILE TO theme/src/config.rs, OTHERWISE IT WILL NOT COMPILE
use raylib::prelude::*;

#[allow(dead_code)]
#[derive(PartialEq)]
pub enum FittingStrategy {
    None,
    Resize,
    RatioScale,
}

pub const INITIAL_WW: i32 = 800;
pub const INITIAL_WH: i32 = 600;

pub const BG: Color = Color::new(200, 255, 255, 255);
pub const FG: Color = Color::new(22, 22, 22, 255);
pub const DIM: Color = Color::new(22, 22, 22, 255);

pub const ENABLE_BG: bool = true;
pub const BG_IMG: &str = "";
pub const BG_DIM: f32 = 0.0;
pub const BG_FIT: FittingStrategy = FittingStrategy::RatioScale;

pub const FONT: &str = "";
