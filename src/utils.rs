use std::path::PathBuf;
use std::{ffi::OsStr, path::Path};
use raylib::prelude::*;
use fm::*;

use crate::FileList;
use crate::preview::Preview;

const MIN_WIDTH: f32 = 400.0;

/// Get filename (and extension) from path
/// (this function doesn't account for a / in the middle of the name)
pub fn get_name(path: &Path) -> String {
    // wtf
    let binding = String::from(path.to_str().unwrap_or("???"));
    let string: Vec<&str> = binding
        .split('/')
        .collect();

    String::from(*string.last().unwrap())
}

pub fn draw_main_panel(d: &mut RaylibDrawHandle, entries: &FileList, tasogare: &Tasogare) {
    d.draw_text_ex(
        tasogare.font.as_ref().unwrap(),
        entries.get_path().to_str().unwrap_or(""),
        Vector2::new(20.0, 5.0),
        30.0,
        0.0,
        theme::FG
    );

    for i in 0..entries.get_count() {
        let entry = get_name(entries.get_entry(i).unwrap());

        if entry.starts_with('.') && !tasogare.show_hidden {
            continue;
        }

        d.draw_text_ex(
            tasogare.font.as_ref().unwrap(),
            &entry,
            Vector2::new(20.0, 40.0 + 20.0 * i as f32),
            20.0,
            0.0,
            theme::FG
        );
    }
}

/// Generate description from file's extension.
pub fn type_from_ext(ext: Option<&OsStr>) -> &str {
    let ext = match ext {
        Some(ext) => ext.to_str().unwrap_or(""),
        None => "",
    };

    match ext {
        "c" | "cc" => "C source file",
        "cpp" | "cxx" => "C++ source file",
        "rs" => "Rust source file",
        "py" => "Python source file",
        "sh" => "Shell script",
        "txt" => "Plain text",

        "exe" | "out" | "elf" => "Executable file",

        "png" | "jpeg" | "jpg" | "tga" => "Image",

        _ => "other",
    }
}

pub fn draw_preview_panel(
    d: &mut RaylibDrawHandle,
    entry: Option<&PathBuf>,
    preview: &Option<Preview>,
    tasogare: &Tasogare
) {
    let line_x = if tasogare.width as f32/2.0 >= MIN_WIDTH {
        tasogare.width as f32/2.0
    } else {
        MIN_WIDTH
    };

    d.draw_line_ex(
        Vector2::new(line_x, 0.0),
        Vector2::new(line_x, tasogare.height as f32),
        6.0,
        theme::FG
    );

    if entry.is_none() {
        d.draw_text_ex(
            tasogare.font.as_ref().unwrap(),
            "Empty directory",
            Vector2::new(tasogare.width as f32/2.0 + 20.0, 5.0),
            30.0,
            0.0,
            theme::FG
        );

        return;
    }

    let entry = entry.unwrap();

    d.draw_text_ex(
        tasogare.font.as_ref().unwrap(),
        &format!(
            "{} ({})",
            entry.file_name().unwrap().to_str().unwrap_or(""),
            type_from_ext(entry.extension())
        ),
        Vector2::new(tasogare.width as f32/2.0 + 20.0, 5.0),
        30.0,
        0.0,
        theme::FG
    );

    match preview {
        Some(Preview::Image(texture)) => {
            let width = tasogare.width / 2 - 40;
            let height = tasogare.height - 80;

            let mut bigger_side = 0;
            let scale =  if height < texture.height {
                let scale = height as f32 / texture.height as f32;
                if (width as f32) < texture.width as f32 * scale {
                    bigger_side = 1;
                    width as f32 / texture.width as f32
                } else {
                    bigger_side = 0;
                    scale
                }
            } else if width < texture.width {
                let scale = width as f32 / texture.width as f32;
                if (height as f32) < texture.height as f32 * scale {
                    bigger_side = 0;
                    height as f32 / texture.height as f32
                } else {
                    bigger_side = 1;
                    scale
                }
            } else {
                1.0
            };

            d.draw_texture_pro(
                texture,
                Rectangle::new(
                    0.0,
                    0.0,
                    texture.width as f32,
                    texture.height as f32
                ),
                if bigger_side == 0 {
                    Rectangle::new(
                        (width + 62) as f32,
                        40.0,
                        texture.width as f32 * scale,
                        height as f32
                    )
                } else {
                    Rectangle::new(
                        (width + 62) as f32,
                        40.0,
                        width as f32,
                        texture.height as f32 * scale
                    )
                },
                Vector2::new(0.0, 0.0),
                0.0,
                Color::WHITE
            )
        }

        Some(Preview::Text(text)) => {
            d.draw_text_ex(
                tasogare.font.as_ref().unwrap(),
                text,
                Vector2::new((tasogare.width/2 + 20) as f32, 40.0),
                20.0,
                0.0,
                theme::FG
            );
        }

        None => {}
    }
}
