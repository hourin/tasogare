use std::{fs, io, path::PathBuf};

#[derive(Clone)]
pub struct FileList {
    path: PathBuf,
    entries: Vec<PathBuf>,
    count: usize,
}

impl FileList {
    pub fn open(path: &str) -> Self {
        FileList {
            path: PathBuf::from(path),
            entries: Vec::default(),
            count: 0,
        }
    }

    pub fn list(&mut self, show_hidden: bool) {
        // We pray 2 times that the directory is valid.
        let mut entries = fs::read_dir(&self.path)
            .unwrap()
            .map(|res| res.map(|e| e.path()))
            .collect::<Result<Vec<_>, io::Error>>()
            .unwrap();

        if !show_hidden {
            entries.retain(|file|
                !file.file_name()
                .unwrap().to_str()
                .unwrap().starts_with(".")
            );
        }

        entries.sort();

        self.entries = entries;
        self.count = self.entries.len();
    }

    pub fn get_path(&self) -> &PathBuf {
        &self.path
    }

    pub fn get_count(&self) -> usize {
        self.count
    }

    pub fn get_entry(&self, index: usize) -> Option<&PathBuf> {
        if index >= self.entries.len() {
            return None;
        }

        Some(&self.entries[index])
    }

    pub fn is_dir(&self, index: usize) -> bool {
        match self.get_entry(index) {
            Some(dir) => dir.is_dir(),
            None => false,
        }
    }

    // These 2 functions should return Result<>, however
    // I'm quite lazy today.

    pub fn chdir(&mut self, new_dir: &str, show_hidden: bool) -> bool {
        self.path = PathBuf::from(new_dir);
        self.list(show_hidden);
        true
    }

    pub fn prevdir(&mut self, show_hidden: bool) -> bool {
        if let Some(parent) = self.path.parent() {
            self.path = parent.to_path_buf();
            self.list(show_hidden);

            return true;
        }

        false
    }
}
