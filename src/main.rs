use home::home_dir;
use raylib::prelude::*;
use fm::*;

mod filelist;
use crate::filelist::*;

mod utils;

fn main() {
    set_trace_log(TraceLogLevel::LOG_ERROR);

    let (mut rl, thread) = raylib::init()
        .size(theme::INITIAL_WW, theme::INITIAL_WH)
        .title("TASOGARE")
        .resizable()
        .vsync()
        .build();

    let mut tasogare = Tasogare::default();
    tasogare.load_font(&mut rl, &thread, theme::FONT);
    tasogare.selection.push(0);

    let mut home = FileList::open(home_dir().unwrap().to_str().unwrap());
    home.list(tasogare.show_hidden);

    let bg_image = theme::load_background(&mut rl, &thread);

    let mut preview: Option<preview::Preview> = None;

    let mut preview_changed: bool = true;

    while !rl.window_should_close() {
        let selection = tasogare.latest_selection();
        if rl.is_key_pressed(KeyboardKey::KEY_H) {
            tasogare.show_hidden = !tasogare.show_hidden;
            home.list(tasogare.show_hidden);
            tasogare.selection.pop();
            tasogare.selection.push(0);
        }

        if rl.is_key_pressed(KeyboardKey::KEY_RIGHT) &&
            home.is_dir(selection as usize) {
            let new_dir = home.get_entry(selection as usize)
                .unwrap()
                .clone();
            if home.chdir(new_dir.to_str().unwrap(), tasogare.show_hidden) {
                tasogare.selection.push(0);
            }
        }

        else if rl.is_key_pressed(KeyboardKey::KEY_LEFT) {
            if home.prevdir(tasogare.show_hidden) && tasogare.selection.len() > 1 {
                tasogare.selection.pop();
            }
        }

        let entry_num = home.get_count() as i64;
        if rl.is_key_pressed(KeyboardKey::KEY_UP) {
            tasogare.change_selection(-1, entry_num);
            preview_changed = true;
        }
        else if rl.is_key_pressed(KeyboardKey::KEY_DOWN) {
            tasogare.change_selection(1, entry_num);
            preview_changed = true;
        }

        if preview_changed {
            preview = Preview::gen(
                &mut rl,
                &thread,
                home.get_entry(selection as usize)
            );
        }

        tasogare.update_size(&rl);

        let mut d = rl.begin_drawing(&thread);
        d.clear_background(theme::BG);

        match &bg_image {
            Some(image) => theme::draw_background(&mut d, image, &tasogare),
            None => {}
        }

        utils::draw_main_panel(&mut d, &home, &tasogare);
        utils::draw_preview_panel(
            &mut d,
            home.get_entry(selection as usize),
            &preview,
            &tasogare
        );
    }
}
